Zadání zápočtového příkladu nanečisto
===================================

### Požadavky:
* Čas na vypracování: _60 minut_
* Program půjde přeložit, nebudou v něm syntaktické chyby
* Máte povoleno používat online/offline dokumentaci
* Máte povoleno používat __VLASTNÍ!!__ kódy
  * Nikoliv kódy někoho jiného nebo referenční implementace
  * Výjimkou jsou ukázky kódu v dokumentaci
* Nesmíte používat žádnou formu komunikace, na jejímž druhém konci je entita, která projde [Turingovým testem](https://cs.wikipedia.org/wiki/Turing%C5%AFv_test)
* Dbejte na dobrou strukturu kódu
* Správným způsobem ošetřujte práci s pamětí a soubory

## Zadání

John Horton Conway byl výborný britský matematik. Jako vzpomínku na jeho neúspěšný boj s COVID-19 máte za úkol naprogramovat jeho nejznámější "hru pro žádné hráče" - buněčný automat [Hra života](https://cs.wikipedia.org/wiki/Hra_%C5%BEivota).

### Popis hry

Hrací plocha se skládá ze čtvercových políček tvořících síť. Každé políčko (buňka) může být živé (černá), nebo mrtvé (bílá). V každém tahu se všechna políčka přebarví podle následující strategie (sousedé jsou políčka, která sousedí alespoň rohem):
- Každá živá buňka s méně než dvěma živými sousedy zemře.
- Každá živá buňka se dvěma nebo třemi živými sousedy zůstává žít.
- Každá živá buňka s více než třemi živými sousedy zemře.
- Každá mrtvá buňka s právě třemi živými sousedy oživne.

V našem případě bude hrací pole konečné, protilehlé hrany na sebe však navazují
*(uvažme plán velikosti `5×5` a políčko `[0;0]`. Jeho sousedy jsou tedy očividně `[0;1]`,`[1;0]`,`[1;1]` a potom sousedící vrchní hranou `[0;4]`, levou hranou `[4;0]` a rohem `[4;4]`)*

*Pozn.: Všechna přebarvení pro všechna políčka se dějí "zároveň" - nejde proto tyto hodnoty měnit "in-place", to je třeba si uvědomit.*

### Váš program

1. Program načte hrací plán (viz **Formát vstupu**)
1. Provede jednu iteraci hry života
1. Vytiskne výstup na `stdout`
1. Počká na stisk klávesy `enter`
1. Bude-li vstup od uživatele něco obsahovat (nestiskne pouze `enter`) program skončí
1. Jinak opakuje od kroku 2.

### Formát vstupu
Program dostane na vstupu vždy jeden argument (neměl by tolerovat více, ani méně), který bude adresou k souboru.
Na jeho prvním řádku jsou 2 čísla oddělená mezerou označující šířku a výšku plánu (v tomto pořadí).
Následují řádky obsahující `#` (černá) a `.` (bílá) popisující políčka plánu. (stejný formát, jako výstup)
Vstup bude vždy korektní, ošetřete však chybu přístupu k souboru.
Testovací soubory najdete ve složce `vstupy`.

### Formát výstupu

Hrací plán se bude vykreslovat, jako ASCII art. Černá políčka budou představována znakem `#`, bílá znakem `.`.

Pozn.: Pokud chcete, můžete pro výpis použít `printf("\u2B1B");` pro černý ⬛ a `printf("\u2B1C");` pro bílý ⬜ emoji čtvereček (vypadá to lépe, ale nejde s nimi pracovat přímo, jako s chary a některé OS, nebo kompilátory s tím mohou mít problém)

Pozn.: Můžete zkusit použít ANSI sekvence (fungují pouze v systémech v POSIX standardu) pro vymazání obsahu obrazovky, ale není to nutné: `printf("\e[1;1H\e[2J");`